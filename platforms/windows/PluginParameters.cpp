namespace aex
{
	extern const char* parameters_json = R"(
[
	{
		"type" : "float_slider",
		"id" : 1000,
		"local_id" : "height_scale",
		"name" : "Height scale",
		"valid_min" : -500.0,
		"valid_max" :  500.0,
		"slider_min" : -500.0,
		"slider_max" :  500.0,
		"default" : 0.0
	},
	{
		"type" : "float_slider",
		"id" : 1010,
		"local_id" : "ao_power",
		"name" : "AO factor",
		"valid_min" : 0.0,
		"valid_max" : 8.0,
		"slider_min" : 0.0,
		"slider_max" : 8.0,
		"default" : 1.0
	},
	{
		"type" : "float_slider",
		"id" : 1011,
		"local_id" : "light_power",
		"name" : "Light factor",
		"valid_min" : 0.0,
		"valid_max" : 4.0,
		"slider_min" : 0.0,
		"slider_max" : 4.0,
		"default" : 1.0
	},
	{
		"type" : "layer",
		"id" : 1012,
		"local_id" : "displacement_map",
		"name" : "Displacement map"
	}
]
)";
}
