#pragma once

namespace aex
{
	extern const char* fullscreenVertexShader;
	extern const char* argbFragmentShader;
	extern const char* testFragmentShader;

	extern const char* defaultVertexShader;
	extern const char* defaultFragmentShader;
}