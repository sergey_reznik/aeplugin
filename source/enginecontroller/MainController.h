#include <et/app/application.h>
#include <et/rendering/rendercontext.h>
#include <et/camera/camera.h>
#include "../shared/Setup.h"

namespace aex
{
	class MainController : public et::IApplicationDelegate
	{
	public:
		void setPluginData(PluginData::Pointer);
		void setParameters(et::Dictionary);
		void setupFrameData();

	private:
		et::ApplicationIdentifier applicationIdentifier() const;
		void setApplicationParameters(et::ApplicationParameters&);
		void applicationDidLoad(et::RenderContext*);
		void applicationWillTerminate();
		void applicationWillResizeContext(const et::vec2i&);
		void render(et::RenderContext*);

	private:
		void validateTargetFramebuffer(et::RenderContext*);
		void performRendering(et::RenderContext*);
		void retrieveImageData(et::RenderContext*);
		void updateInputTextures(et::RenderContext*);
		void validateMesh(et::RenderContext*, const et::vec2i&);

	private:
		et::Framebuffer::Pointer _mainFramebuffer;
		et::Program::Pointer _argbProgram;
		et::Program::Pointer _testProgram;

		std::map<std::string, et::Texture::Pointer> _inputTextures;

		et::VertexArrayObject _vao;
		et::Program::Pointer _defaultProgram;
		et::Camera _camera;

		et::vec2i _targetFrameSize;
		et::vec2i _cachedMeshSize;
		PluginData::Pointer _data;
		et::Dictionary _parameters;
	};
}
