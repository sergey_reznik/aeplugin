#include "MainController.h"
#include "CommonShaders.h"

#include "../conversion/layer-texture.h"

#include <et/geometry/geometry.h>
#include <et/opengl/opengl.h>
#include <et/primitives/primitives.h>
#include <AE_Macros.h>
#include <AE_EffectCB.h>
#include <AEGP_SuiteHandler.h>
#include <Smart_Utils.h>

using namespace aex;
using namespace et;

IApplicationDelegate* et::Application::initApplicationDelegate() 
{ 
	return sharedObjectFactory().createObject<MainController>(); 
}

ApplicationIdentifier MainController::applicationIdentifier() const
{
	return ApplicationIdentifier("com.sergeyreznik.aeplugin", "Sergey Reznik", "AEPlugin");
}

void MainController::setApplicationParameters(et::ApplicationParameters& params)
{
	params.windowStyle = WindowStyle_Hidden;
	params.shouldCreateContext = true;
	params.shouldCreateRunLoop = false;
	params.shouldPreserveRenderContext = true;
}

void MainController::applicationDidLoad(et::RenderContext* rc)
{
	PreservedRenderStateScope scope(rc, true);

	_argbProgram = rc->programFactory().genProgram("argb-program", fullscreenVertexShader, argbFragmentShader);
	_argbProgram->setUniform("input_texture", 0);
	_testProgram = rc->programFactory().genProgram("test-program", fullscreenVertexShader, testFragmentShader);
	_testProgram->setUniform("input_texture", 0);

	_defaultProgram = rc->programFactory().genProgram("test-program", defaultVertexShader, defaultFragmentShader);

	_camera.perspectiveProjection(QUARTER_PI, 1.0, 1.0, 10024.0f);
	_camera.lookAt(vec3(10.0f));
}

void MainController::applicationWillResizeContext(const et::vec2i&)
{

}

void MainController::render(et::RenderContext* rc)
{
	checkOpenGLError("render - begin");
	validateTargetFramebuffer(rc);
	updateInputTextures(rc);

	performRendering(rc);

	retrieveImageData(rc);
	checkOpenGLError("render - end");
}

void MainController::performRendering(RenderContext* rc) 
{
	validateMesh(rc, _targetFrameSize / 2);

	auto& rs = rc->renderState();
	rs.setBlend(false, BlendState::Default, true);
	rs.setDepthMask(true, true);
	rs.setDepthTest(true, true);
	rs.bindFramebuffer(_mainFramebuffer, true);
	_mainFramebuffer->setCurrentRenderTarget(0);

	rs.setDepthMask(true, true);
	rc->renderer()->clear(true, true);

	auto displaceTexture = _inputTextures["displacement_map"];
	auto sourceTexture = _inputTextures["source"];

	rs.bindTexture(0, sourceTexture);
	rs.bindTexture(1, displaceTexture);
	rs.bindProgram(_defaultProgram, true);
	rs.bindVertexArray(_vao, true);

	_camera.perspectiveProjection(QUARTER_PI, vector2ToFloat(_mainFramebuffer->size()).aspect(), 1.0f, 8192.0f);
	_defaultProgram->setCameraProperties(_camera);
	_defaultProgram->setUniform("texel", displaceTexture.valid() ? displaceTexture->texel() : vec2(1.0f));

	_defaultProgram->setUniform("vertexScale", vec3(float(_targetFrameSize.x), float(_targetFrameSize.y), 
		_parameters.dictionaryForKey("height_scale").floatForKey("value")->content));

	_defaultProgram->setUniform("aoPower",
		_parameters.dictionaryForKey("ao_power").floatForKey("value")->content);

	_defaultProgram->setUniform("lightPower", 
		_parameters.dictionaryForKey("light_power").floatForKey("value")->content);

	rc->renderer()->drawAllElements(_vao->indexBuffer());
}

void MainController::validateTargetFramebuffer(RenderContext* rc)
{
	if (_mainFramebuffer.invalid())
	{
		_mainFramebuffer = rc->framebufferFactory().createFramebuffer(_targetFrameSize, TextureTarget::Texture_2D, "main_framebuffer");
		_mainFramebuffer->addSameRendertarget();
	}
	else if (_targetFrameSize != _mainFramebuffer->size())
	{
		_mainFramebuffer->resize(_targetFrameSize);
	}
}

void MainController::updateInputTextures(et::RenderContext* rc)
{
	auto texDescription = layerToTextureDescription(_data->params[0], _data->inData);
	_inputTextures["source"] = rc->textureFactory().genTexture(texDescription);

	for (auto pp : _parameters->content)
	{
		Dictionary param = pp.second;
		if (param.stringForKey("type")->content == "layer")
		{
			PF_ParamDef layer = { };

			int64_t index = param.integerForKey("index_in_parameters")->content;
			PF_CHECKOUT_PARAM(_data->inData, index, _data->inData->current_time, 
				_data->inData->time_step, _data->inData->time_scale, &layer);

			std::string localId = param.stringForKey("local_id")->content;
			texDescription = layerToTextureDescription(&layer, _data->inData);

			if (texDescription.valid())
				_inputTextures[localId] = rc->textureFactory().genTexture(texDescription);

			PF_CHECKIN_PARAM(_data->inData, &layer);
		}
	}
}

void MainController::retrieveImageData(RenderContext* rc)
{
	auto in_data = _data->inData;
	auto outputLayer = _data->output;
	auto& rs = rc->renderState();
	auto rn = rc->renderer();

	auto dataType = PF_WORLD_IS_DEEP(outputLayer) ? DataType::UnsignedShort : DataType::UnsignedChar;
	auto bitsPerPixel = 4 * bitsPerPixelForType(dataType);
	auto rowLength = 8 * outputLayer->rowbytes / bitsPerPixel;

	rs.bindFramebuffer(_mainFramebuffer, true);
	_mainFramebuffer->setCurrentRenderTarget(1);

	rs.bindTexture(0, _mainFramebuffer->renderTarget(0));
	rs.bindProgram(_argbProgram);
	_argbProgram->setUniform("bitsPerPixelScale", float(bitsPerPixel / 32));
	rn->fullscreenPass();

	ET_ASSERT(outputLayer->width = _targetFrameSize.x);
	ET_ASSERT(outputLayer->height = _targetFrameSize.x);

	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_ROW_LENGTH, rowLength);

	BinaryDataStorage imageData(reinterpret_cast<unsigned char*>(outputLayer->data),
		outputLayer->rowbytes * outputLayer->height);

	rn->readFramebufferData(_mainFramebuffer->size(), TextureFormat::RGBA, dataType, imageData);
}

void MainController::applicationWillTerminate()
{

}

void MainController::setPluginData(PluginData::Pointer pd)
{
	_data = pd;
}

void MainController::setupFrameData()
{
	auto in_data = _data->inData;
	AEGP_SuiteHandler suites(in_data->pica_basicP);

	A_Time compTime = { };
	AEGP_LayerH cameraLayer = {};

	suites.PFInterfaceSuite1()->AEGP_ConvertEffectToCompTime(in_data->effect_ref, 
		in_data->current_time, in_data->time_scale, &compTime);

	auto err = suites.PFInterfaceSuite1()->AEGP_GetEffectCamera(in_data->effect_ref,
		&compTime, &cameraLayer);

	if (!err && cameraLayer)
	{
		A_Matrix4 transform = { };
		AEGP_StreamVal streamVal = {};

		suites.LayerSuite5()->AEGP_GetLayerToWorldXform(cameraLayer, &compTime, &transform);

		suites.StreamSuite2()->AEGP_GetLayerStreamValue(cameraLayer, AEGP_LayerStream_ZOOM, 
			AEGP_LTimeMode_CompTime, &compTime, 0, &streamVal, nullptr);

		mat4 mv;
		mv[0] = vec4(transform.mat[0][0], transform.mat[0][1], transform.mat[0][2], transform.mat[0][3]);
		mv[1] = vec4(transform.mat[1][0], transform.mat[1][1], transform.mat[1][2], transform.mat[1][3]);
		mv[2] = vec4(transform.mat[2][0], transform.mat[2][1], transform.mat[2][2], transform.mat[2][3]);
		mv[3] = vec4(transform.mat[3][0], transform.mat[3][1], transform.mat[3][2], transform.mat[3][3]);
		_camera.setModelViewMatrix(mv);
	}

	_targetFrameSize = vec2i(_data->outData->width, _data->outData->height);
}

void MainController::validateMesh(et::RenderContext* rc, const et::vec2i& sz)
{
	if (_vao.valid() && (_cachedMeshSize == sz)) return;

	_cachedMeshSize = sz;

	VertexDeclaration decl(true, VertexAttributeUsage::Position, VertexAttributeType::Vec3);
	decl.push_back(VertexAttributeUsage::Normal, VertexAttributeType::Vec3);
	decl.push_back(VertexAttributeUsage::TexCoord0, VertexAttributeType::Vec3);
	VertexArray::Pointer va = VertexArray::Pointer::create(decl, 0);
	primitives::createSquarePlane(va, unitZ, vec2(0.5f), _cachedMeshSize);

	IndexArray::Pointer ia = IndexArray::Pointer::create(IndexArrayFormat::Format_32bit, 
		primitives::indexCountForRegularMesh(_cachedMeshSize, PrimitiveType::TriangleStrips), PrimitiveType::TriangleStrips);
	primitives::buildTriangleStripIndexes(ia, _cachedMeshSize, 0, 0);

	_vao = rc->vertexBufferFactory().createVertexArrayObject("mesh", va, BufferDrawType::Static,
		ia, BufferDrawType::Static);
}

void MainController::setParameters(et::Dictionary p)
{
	_parameters = p;
}
