#include "CommonShaders.h"

extern const char* aex::fullscreenVertexShader = R"(
etVertexIn vec2 Vertex;
etVertexOut vec2 TexCoord;
void main()
{
	TexCoord = 0.5 + 0.5 * Vertex;
	gl_Position = vec4(Vertex, 0.0, 1.0);
}
)";

extern const char* aex::argbFragmentShader = R"(
uniform sampler2D input_texture;
uniform float bitsPerPixelScale = 1.0;
etFragmentIn vec2 TexCoord;
void main()
{
	vec4 sampledColor = etTexture2D(input_texture, TexCoord);
	etFragmentOut = vec4(sampledColor.w, bitsPerPixelScale * sampledColor.w * sampledColor.xyz);
}
)";

extern const char* aex::testFragmentShader = R"(
uniform sampler2D input_texture;
etFragmentIn vec2 TexCoord;
void main()
{
	vec4 sampledOffset = etTexture2D(input_texture, TexCoord).yzwx;

	vec2 offsetCoord = 
		mod(sampledOffset.xy / (sampledOffset.z + 0.00001), 1.0) + 
		mod(sampledOffset.yz / (sampledOffset.x + 0.00001), 1.0);
		mod(sampledOffset.zx / (sampledOffset.y + 0.00001), 1.0);

	offsetCoord = mix(offsetCoord / 3.0, TexCoord, 0.25);

	etFragmentOut = etTexture2D(input_texture, offsetCoord).yzwx;
}
)";

extern const char* aex::defaultVertexShader = R"(
layout (binding = 1) uniform sampler2D displacement_texture;
uniform mat4 mModelViewProjection;
uniform vec3 vertexScale;
uniform vec2 texel;
etVertexIn vec3 Vertex;
etVertexIn vec3 Normal;
etVertexIn vec2 TexCoord0;
etVertexOut vec3 vNormalWS;
etVertexOut vec2 TexCoord;
etVertexOut float ambientOcclusion;

float sampleHeight(in vec2 coordinate)
{
	return vertexScale.z * dot(texture(displacement_texture, coordinate), vec4(0.25));
}

vec3 sampleNormal(in vec2 coordinate)
{
	float normalScale = max(texel.x, texel.y);

	float hc = sampleHeight(coordinate);

	float hmy = sampleHeight(coordinate - vec2(0.0, texel.y));
	float hmx = sampleHeight(coordinate - vec2(texel.x, 0.0));
	float hpx = sampleHeight(coordinate + vec2(texel.x, 0.0));
	float hpy = sampleHeight(coordinate + vec2(0.0, texel.y));

	vec3 nf = cross
	(
		vec3(texel.x, 0.0, normalScale * (hpx - hc)), 
		vec3(0.0, texel.y, normalScale * (hpy - hc))
	);

	vec3 nb = cross
	(
		vec3(texel.x, 0.0, normalScale * (hc - hmx)), 
		vec3(0.0, texel.y, normalScale * (hc - hmy))
	);

	return normalize(nf + nb);
}

float computeAmbientOcclusion(in vec2 coordinate, float radius)
{
	const float stepSize = 1.5;

	float h0 = sampleHeight(coordinate);

	float occlusion = 0.0;
	float occlusionExp = -0.025;

	float samples = 0.0;
	float y = -radius;
	while (y <= radius)
	{
		float x = -radius;
		while (x <= radius)
		{
			float sampledHeight = sampleHeight(coordinate + stepSize * vec2(x, y) * texel);
			occlusion += 1.0 - exp(occlusionExp * max(0.0, sampledHeight - h0));
			samples += 1.0;
			x += 1.0;
		}
		y += 1.0;
	}

	return 1.0 - occlusion / samples;	
}

void main()
{
	TexCoord = 1.0 - TexCoord0;

	float h = sampleHeight(TexCoord);
	vNormalWS = sampleNormal(TexCoord);
	ambientOcclusion = computeAmbientOcclusion(TexCoord, 20.0);

	vec3 finalVertex = vec3(vertexScale.xy * Vertex.xy, Vertex.z + h);

	gl_Position = mModelViewProjection * vec4(finalVertex, 1.0);
}
)";

extern const char* aex::defaultFragmentShader = R"(
layout (binding = 0) uniform sampler2D input_texture;
uniform float aoPower;
uniform float lightPower;
etFragmentIn vec3 vNormalWS;
etFragmentIn vec2 TexCoord;
etFragmentIn float ambientOcclusion;

const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));

void main()
{
	float ao = pow(ambientOcclusion, aoPower);
	float LdotN = pow(0.5 + 0.5 * max(0.0, dot(vNormalWS, lightDirection)), lightPower);

	etFragmentOut = texture(input_texture, TexCoord).yzwx * (ao * LdotN);
	etFragmentOut.w = 1.0;
}
)";
