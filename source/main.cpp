#include "shared/Setup.h"
#include "shared/AfterEffects.h"

using namespace aex;

extern "C"
{
	DllExport PF_Err main(PF_Cmd cmd, PF_InData* in_data, PF_OutData* out_data, PF_ParamDef* params[], PF_LayerDef* output, void* extra)
	{
		PluginData::Pointer data = PluginData::Pointer::create(cmd, in_data, out_data, params, output, extra);

		try
		{
			AfterEffects::instance().processCommand(data);
		}
		catch (PF_Err& thrown_err)
		{
			data->result = thrown_err;
		}
		catch (...)
		{
			data->result = PF_Err_INTERNAL_STRUCT_DAMAGED;
		}

		return data->result;
	}
}
