#include "ParametersHandler.h"
#include <Param_Utils.h>
#include <AE_EffectCB.h>
#include <et/json/json.h>

namespace aex { extern const char* parameters_json; }

using namespace et;
using namespace aex;

const std::map<A_long, std::string> parameterTypeToString = 
{
	{ PF_Param_LAYER, "layer" },
	{ PF_Param_SLIDER,	"slider" },
	{ PF_Param_FIX_SLIDER, "fix_slider" },
	{ PF_Param_ANGLE, "angle" },
	{ PF_Param_CHECKBOX, "checkbox" },
	{ PF_Param_COLOR, "color" },
	{ PF_Param_POINT, "point" },
	{ PF_Param_POPUP, "popup" },
	{ PF_Param_FLOAT_SLIDER, "float_lider" }
};

ParametersHandler::ParametersHandler()
{
	ValueClass vc = ValueClass_Invalid;
	_pluginParameters = json::deserialize(parameters_json, vc);
	ET_ASSERT(vc == ValueClass_Array);

	_addParameterFunctions["float_slider"] = [this](PluginData::Pointer pd, et::Dictionary values) -> PF_Err
	{
		PF_ParamDef def = {};
		auto in_data = pd->inData;
		A_long paramId(values.integerForKey("id")->content);

		std::string a_name = values.stringForKey("name")->content;
		float a_valid_min = values.floatForKey("valid_min")->content;
		float a_valid_max = values.floatForKey("valid_max")->content;
		float a_slider_min = values.floatForKey("slider_min")->content;
		float a_slider_max = values.floatForKey("slider_max")->content;
		float a_curve_tolerance = values.floatForKey("curve_tolerance")->content;
		float a_default = values.floatForKey("default")->content;
		float a_precision = values.floatForKey("precision")->content;

		PF_ADD_FLOAT_SLIDER(a_name.c_str(), a_valid_min, a_valid_max, a_slider_min, a_slider_max,
			a_curve_tolerance, a_default, a_precision, 0, 0, paramId);

		_parameters[pd->outData->num_params] = values;
		++pd->outData->num_params;
		return PF_Err_NONE;
	};

	_addParameterFunctions["checkbox"] = [this](PluginData::Pointer pd, et::Dictionary values) -> PF_Err
	{
		PF_ParamDef def = {};
		auto in_data = pd->inData;
		A_long paramId(values.integerForKey("id")->content);

		PF_ADD_CHECKBOX
		(
			values.stringForKey("name")->content.c_str(), 
			values.stringForKey("second_name")->content.c_str(), 
			static_cast<PF_ParamValue>(values.integerForKey("default")->content),
			0, // TODO: display flags
			paramId
		);

		_parameters[pd->outData->num_params] = values;
		++pd->outData->num_params;
		return PF_Err_NONE;
	};

	_addParameterFunctions["angle"] = [this](PluginData::Pointer pd, et::Dictionary values) -> PF_Err
	{
		PF_ParamDef def = {};
		auto in_data = pd->inData;
		A_long paramId(values.integerForKey("id")->content);

		PF_ADD_ANGLE
		(
			values.stringForKey("name")->content.c_str(),
			values.floatForKey("default")->content,
			paramId
		);

		_parameters[pd->outData->num_params] = values;
		++pd->outData->num_params;
		return PF_Err_NONE;
	};

	_addParameterFunctions["layer"] = [this](PluginData::Pointer pd, et::Dictionary values) -> PF_Err
	{
		PF_ParamDef def = {};
		auto in_data = pd->inData;

		PF_ADD_LAYER
		(
			values.stringForKey("name")->content.c_str(), 
			0, 
			A_long(values.integerForKey("id")->content)
		);

		_parameters[pd->outData->num_params] = values;
		++pd->outData->num_params;
		return PF_Err_NONE;
	};
}

void ParametersHandler::addDefaultParameters(PluginData::Pointer pd)
{
	addParametersFromArray(pd, _pluginParameters);
}

void ParametersHandler::addParametersFromArray(PluginData::Pointer pd, et::ArrayValue values)
{
	pd->outData->num_params = 1;

	for (auto obj : values->content)
	{
		ET_ASSERT(obj->valueClass() == ValueClass_Dictionary);
		Dictionary param = obj;

		ET_ASSERT(param.hasKey("id") && param.objectForKey("id")->valueClass() == ValueClass_Integer);
		ET_ASSERT(param.hasKey("local_id") && param.objectForKey("local_id")->valueClass() == ValueClass_String);

		auto paramType = param.stringForKey("type")->content;
		if (_addParameterFunctions.count(paramType) > 0)
		{
			PF_Err err = _addParameterFunctions.at(paramType)(pd, param);

			if (err != PF_Err_NONE)
				throw err;
		}
	}
}

et::Dictionary ParametersHandler::parseParameters(PluginData::Pointer data)
{
	Dictionary result;

	size_t i = 1;
	while (data->params[i])
	{
		if (_parameters.count(i) > 0)
		{
			Dictionary value;
			value->content = _parameters[i]->content;
			value.setIntegerForKey("index_in_parameters", i);
			parseParameter(data->params[i], value);
			result.setDictionaryForKey(value.stringForKey("local_id")->content, value);
		}
		++i;
	}

	return result;
}

void ParametersHandler::parseParameter(PF_ParamDef* param, et::Dictionary& result)
{
	if (param->param_type == PF_Param_FLOAT_SLIDER)
	{
		result.setFloatForKey("value", param->u.fs_d.value);
	}
}
