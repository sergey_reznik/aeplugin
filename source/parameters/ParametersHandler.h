#pragma once

#include "../shared/Setup.h"

namespace aex
{
	class ParametersHandler
	{
	public:
		ParametersHandler();

		void addDefaultParameters(PluginData::Pointer);
		void addParametersFromArray(PluginData::Pointer, et::ArrayValue);

		et::Dictionary parseParameters(PluginData::Pointer);

	private:
		ET_DENY_COPY(ParametersHandler);

		void parseParameter(PF_ParamDef*, et::Dictionary&);

	private:
		et::ArrayValue _pluginParameters;
		std::map<std::string, std::function<PF_Err(PluginData::Pointer, et::Dictionary)>> _addParameterFunctions;
		std::map<uint64_t, et::Dictionary> _parameters;
	};
}
