#pragma once

#define PF_TABLE_BITS	12
#define PF_TABLE_SZ_16	4096

#include <et/core/et.h>

#include <AEConfig.h>
#if defined(AE_OS_WIN)
#	include <Windows.h>
#endif
#include <entry.h>
#include <AE_Effect.h>

namespace aex
{
	struct PluginData : public et::Shared
	{
	public:
		ET_DECLARE_POINTER(PluginData)

	public:
		PF_InData* inData = nullptr;
		PF_OutData* outData = nullptr;
		PF_ParamDef** params = nullptr;
		PF_LayerDef* output = nullptr;
		void* extra = nullptr;

		uint32_t command = 0;
		uint32_t result = 0;

		PluginData(uint32_t cmd, PF_InData* aIn, PF_OutData* aOut, PF_ParamDef** aParams, PF_LayerDef* aLayer, void* e) :
			command(cmd), inData(aIn), outData(aOut), params(aParams), output(aLayer), extra(e) { }
	};

	typedef std::function<void(PluginData::Pointer)> CommandHandlerFunction;
}
