#pragma once

#include <functional>
#include <et/app/application.h>
#include <et/app/applicationnotifier.h>
#include "AfterEffects.h"

using namespace et;
using namespace aex;

AfterEffects::AfterEffects()
{
	_handlers[PF_Cmd_GLOBAL_SETUP] = std::bind(&AfterEffects::handleGlobalSetup, this, std::placeholders::_1);
	_handlers[PF_Cmd_GLOBAL_SETDOWN] = std::bind(&AfterEffects::handleGlobalSetdown, this, std::placeholders::_1);

	_handlers[PF_Cmd_PARAMS_SETUP] = std::bind(&AfterEffects::handleParametersSetup, this, std::placeholders::_1);

	_handlers[PF_Cmd_FRAME_SETUP] = std::bind(&AfterEffects::handleFrameSetup, this, std::placeholders::_1);
	_handlers[PF_Cmd_RENDER] = std::bind(&AfterEffects::handleRender, this, std::placeholders::_1);

	_handlers[PF_Cmd_COMPLETELY_GENERAL] = std::bind(&AfterEffects::completelyGeneral, this, std::placeholders::_1);
	_handlers[PF_Cmd_QUERY_DYNAMIC_FLAGS] = std::bind(&AfterEffects::queryDynamicFlags, this, std::placeholders::_1);

	/*
	_handlers[PF_Cmd_SMART_PRE_RENDER] = std::bind(&AfterEffects::handleFrameSetup, this, std::placeholders::_1);
	_handlers[PF_Cmd_SMART_RENDER] = std::bind(&AfterEffects::handleRender, this, std::placeholders::_1);
	*/
}

ParametersHandler& AfterEffects::parametersHandler()
{
	return _parametersHandler;
}

void AfterEffects::processCommand(PluginData::Pointer pd)
{
	if (_handlers.count(pd->command) == 0)
	{
		log::info("Unhandled command: %u", pd->command);
		return;
	}

	_handlers.at(pd->command)(pd);
}

void AfterEffects::handleGlobalSetup(PluginData::Pointer pd)
{
	pd->outData->my_version = PF_VERSION(1, 0, 0, 0, 0);

	pd->outData->out_flags = PF_OutFlag_DEEP_COLOR_AWARE;
	pd->outData->out_flags2 = PF_OutFlag2_SUPPORTS_QUERY_DYNAMIC_FLAGS |
		PF_OutFlag2_I_USE_3D_CAMERA | PF_OutFlag2_I_USE_3D_LIGHTS; // PF_OutFlag2_FLOAT_COLOR_AWARE | PF_OutFlag2_SUPPORTS_SMART_RENDER;

	et::application().run(0, nullptr);
	_mainController = static_cast<MainController*>(application().delegate());
}

void AfterEffects::handleGlobalSetdown(PluginData::Pointer pd)
{
	application().quit(0);
}

void AfterEffects::handleParametersSetup(PluginData::Pointer pd)
{
	_parametersHandler.addDefaultParameters(pd);
}

void AfterEffects::handleFrameSetup(PluginData::Pointer pd)
{
	ET_ASSERT(_mainController);

	_mainController->setPluginData(pd);
	_mainController->setParameters(_parametersHandler.parseParameters(pd));
	_mainController->setupFrameData();
}

void AfterEffects::handleRender(PluginData::Pointer pd)
{
	ET_ASSERT(_mainController);
	_mainController->setPluginData(pd);

	ApplicationNotifier().notifyUpdate();
}

void AfterEffects::completelyGeneral(PluginData::Pointer)
{

}

void AfterEffects::queryDynamicFlags(PluginData::Pointer)
{

}
