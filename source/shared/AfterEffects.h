#pragma once

#include <et/core/singleton.h>

#include "Setup.h"
#include "../parameters/ParametersHandler.h"
#include "../enginecontroller/MainController.h"

namespace aex
{
	class AfterEffects : public et::Singleton<AfterEffects>
	{
	public:
		ParametersHandler& parametersHandler();

		void processCommand(PluginData::Pointer);

	private:
		void handleGlobalSetup(PluginData::Pointer);
		void handleGlobalSetdown(PluginData::Pointer);
		void handleParametersSetup(PluginData::Pointer);

		void handleFrameSetup(PluginData::Pointer);
		void handleRender(PluginData::Pointer);

		void completelyGeneral(PluginData::Pointer);
		void queryDynamicFlags(PluginData::Pointer);

	private:
		AfterEffects();
		ET_SINGLETON_COPY_DENY(AfterEffects)

	private:
		ParametersHandler _parametersHandler;
		MainController* _mainController = nullptr;
		std::map<uint32_t, CommandHandlerFunction> _handlers;
	};
}
