#include <AEGP_SuiteHandler.h>
#include "layer-texture.h"

using namespace et;
using namespace aex;

/* For source, extent_hint is the smallest rect encompassing all opaque
* (non-zero alpha) areas of the layer.  For output, this encompasses
* the area that needs to be rendered (i.e. not covered by other layers,
* needs refreshing, etc.). if your plug-in varies based on extent (like
* a diffusion dither, for example) you should ignore this param and
* render the full frame each time.
*/

/*
PF_WorldFlags		world_flags;
PF_PixelPtr			data;
A_long				rowbytes;
A_long				width;
A_long				height;
PF_UnionableRect	extent_hint;
PF_RationalScale	pix_aspect_ratio;	/* pixel aspect ratio of checked out layer *
A_long				origin_x;			/* origin of buffer in layer coords; smart effect checkouts only *
A_long				origin_y;
A_long				dephault;			/* use a PF_LayerDefault constant defined above *
*/

TextureDescription::Pointer aex::layerToTextureDescription(PF_ParamDef* inputParam, PF_InData* in_data)
{
	PF_LayerDef* inputLayer = &inputParam->u.ld;

	if ((inputLayer->width == 0) || (inputLayer->height == 0))
		return TextureDescription::Pointer();

	PF_Pixel8* p8 = nullptr;
	PF_Pixel16* p16 = nullptr;
	PF_GET_PIXEL_DATA8(inputLayer, nullptr, &p8);
	PF_GET_PIXEL_DATA16(inputLayer, nullptr, &p16);

	TextureDescription::Pointer result = TextureDescription::Pointer::create();
	result->channels = 1;
	result->alignment = 1;
	result->mipMapCount = 1;
	result->layersCount = 1;
	result->target = TextureTarget::Texture_2D;
	result->size = vec2i(inputLayer->width, inputLayer->height);

	if (p8)
	{
		result->data = BinaryDataStorage(reinterpret_cast<unsigned char*>(p8), inputLayer->rowbytes * inputLayer->height);
		result->bitsPerPixel = 32;
		result->internalformat = TextureFormat::RGBA8;
		result->format = TextureFormat::RGBA;
		result->type = DataType::UnsignedChar;
	}
	else if (p16)
	{
		result->data = BinaryDataStorage(reinterpret_cast<unsigned char*>(p16), inputLayer->rowbytes * inputLayer->height);
		result->bitsPerPixel = 64;
		result->internalformat = TextureFormat::RGBA16;
		result->format = TextureFormat::RGBA;
		result->type = DataType::UnsignedShort;
	}
	else // assuming 32f data
	{
		result->data = BinaryDataStorage(reinterpret_cast<unsigned char*>(inputLayer->data), inputLayer->rowbytes * inputLayer->height);
		result->bitsPerPixel = 128;
		result->internalformat = TextureFormat::RGBA32F;
		result->format = TextureFormat::RGBA;
		result->type = DataType::Float;
	}

	result->rowSize = 8 * inputLayer->rowbytes / result->bitsPerPixel;
	return result;
}
