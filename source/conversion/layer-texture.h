#pragma once

#include <et/imaging/texturedescription.h>
#include "../shared/Setup.h"

namespace aex
{
	et::TextureDescription::Pointer layerToTextureDescription(PF_ParamDef*, PF_InData*);
}
