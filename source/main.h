#include "shared/Setup.h"

extern "C"
{
	DllExport PF_Err main(PF_Cmd cmd, PF_InData *in_data, PF_OutData *out_data, 
		PF_ParamDef *params[], PF_LayerDef *output, void *extra);
}
